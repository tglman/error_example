use error_example::{operation1, operation2, AllErrors, Error1, Error2, LE};

#[derive(Debug)]
enum UserError {
    LibAllErrors(AllErrors),
    UtfError(std::str::Utf8Error),
}

impl<T: Into<AllErrors>> From<LE<T>> for UserError {
    fn from(all: LE<T>) -> Self {
        UserError::LibAllErrors(all.error().into())
    }
}

impl From<std::str::Utf8Error> for UserError {
    fn from(all: std::str::Utf8Error) -> Self {
        UserError::UtfError(all)
    }
}

fn forward_error() -> Result<(), UserError> {
    operation1()?;
    operation2()?;
    Ok(())
}

fn handle_error1() {
    // This case need to handle only the operation1 errors ignoring all the rest
    match operation1() {
        Ok(_) => {}
        Err(LE::LE(Error1::Error1)) => {
            println!("handled error 1");
        }
    }
}

fn handle_error2() {
    // This case need to handle only the operation2 errors ignoring all the rest
    match operation2() {
        Ok(_) => {}
        Err(LE::LE(Error2::Error2)) => {
            println!("handled error 2");
        }
        Err(LE::LE(Error2::Error3(msg))) => {
            println!("handled error 3 {}", msg);
        }
    }
}

fn main() -> Result<(), UserError> {
    handle_error1();
    handle_error2();
    forward_error()?;
    Ok(())
}
