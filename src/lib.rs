use std::convert::From;
use thiserror::Error;

pub enum LE<T: Into<AllErrors>> {
    LE(T),
}

impl<T: Into<AllErrors>> LE<T> {
    pub fn error(self) -> T {
        match self {
            LE::LE(a) => a,
        }
    }
}

#[derive(Debug, Error)]
pub enum AllErrors {
    #[error("Error1")]
    Error1,
    #[error("Error2")]
    Error2,
    #[error("Error3:{0}")]
    Error3(String),
}

#[derive(Debug, Error)]
pub enum Error1 {
    #[error("Error1")]
    Error1,
}

impl From<Error1> for AllErrors {
    fn from(_: Error1) -> Self {
        AllErrors::Error1
    }
}

#[derive(Debug, Error)]
pub enum Error2 {
    #[error("Error2")]
    Error2,
    #[error("Error3: {0}")]
    Error3(String),
}

impl From<Error2> for AllErrors {
    fn from(error2: Error2) -> Self {
        match error2 {
            Error2::Error2 => AllErrors::Error2,
            Error2::Error3(msg) => AllErrors::Error3(msg),
        }
    }
}

pub fn operation1() -> Result<(), LE<Error1>> {
    Err(LE::LE(Error1::Error1))
}

pub fn operation2() -> Result<(), LE<Error2>> {
    Err(LE::LE(Error2::Error2))
}

pub fn operation3() -> Result<(), LE<Error2>> {
    Err(LE::LE(Error2::Error3("A error happened".to_owned())))
}
